<?php

declare(strict_types=1);

namespace CommissionTask\Exceptions;

use Exception;

class ProcessRowException extends Exception
{
}
