<?php

declare(strict_types=1);

namespace CommissionTask\Exceptions;

use Exception;

class DataReadException extends Exception
{
}
