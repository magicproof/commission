<?php

declare(strict_types=1);

namespace CommissionTask\Exceptions;

class ExtensionException extends DataReadException
{
}
