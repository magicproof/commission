<?php

declare(strict_types=1);

namespace CommissionTask\Exceptions;

class FileNotFoundException extends DataReadException
{
}
