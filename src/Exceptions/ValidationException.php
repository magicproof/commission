<?php

declare(strict_types=1);

namespace CommissionTask\Exceptions;

class ValidationException extends ProcessRowException
{
}
