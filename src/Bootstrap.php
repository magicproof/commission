<?php

declare(strict_types=1);

namespace CommissionTask;

use DI\Container;
use DI\ContainerBuilder;

class Bootstrap
{
    protected static ?Container $container = null;

    public function resetContainer()
    {
        self::$container = null;
    }

    public function getApp(): App
    {
        return $this->getContainer()->get(App::class);
    }

    public function getContainer(): Container
    {
        if (!self::$container) {
            $containerBuilder = new ContainerBuilder();
            $containerBuilder->addDefinitions(require 'config/definitions.php');
            $containerBuilder->useAutowiring(true);
            self::$container = $containerBuilder->build();
        }

        return self::$container;
    }
}
