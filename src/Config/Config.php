<?php

declare(strict_types=1);

namespace CommissionTask\Config;

use CommissionTask\Contracts\Config\ConfigInterFace;

class Config implements ConfigInterFace
{
    public function __construct(
        protected array $config,
    ) {
    }

    public function get(string $key): mixed
    {
        $keys = explode('.', $key);
        $value = $this->config;
        do {
            $value = $value[current($keys)] ?? null;
        } while (next($keys));

        return $value;
    }

    public function getTransactionFieldOrderKey(string $fieldName): ?int
    {
        return $this->get('transaction_field_order.'.$fieldName);
    }
}
