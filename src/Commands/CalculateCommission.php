<?php

declare(strict_types=1);

namespace CommissionTask\Commands;

use CommissionTask\Bootstrap;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CalculateCommission extends Command
{
    protected function configure()
    {
        $this->setName('calculate')
            ->setDescription('Calculate commission for transactions')
            ->addArgument('filepath', InputArgument::REQUIRED, 'The path to file with transactions.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $bootstrap = new Bootstrap();
        $container = $bootstrap->getContainer();
        $container->set(OutputInterface::class, $output);
        $bootstrap->getApp()->handle($input->getArgument('filepath'));

        return Command::SUCCESS;
    }
}
