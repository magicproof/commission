<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Rules;

use CommissionTask\Config\Config;

abstract class AbstractRule
{
    public function __construct(
        protected int $key,
        protected ?string $value,
        protected ?Config $config = null
    ) {
    }
}
