<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Rules;

interface RuleInterface
{
    public function passes(): bool;

    public function getMessage(): string;
}
