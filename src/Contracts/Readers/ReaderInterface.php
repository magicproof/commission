<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Readers;

use Generator;
use SplFileObject;

interface ReaderInterface
{
    public function getRows(): Generator;

    public function setFile(SplFileObject $file): self;
}
