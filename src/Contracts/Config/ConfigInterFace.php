<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Config;

interface ConfigInterFace
{
    public function get(string $key): mixed;

    public function getTransactionFieldOrderKey(string $fieldName): ?int;
}
