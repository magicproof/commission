<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services;

use CommissionTask\Entities\Operation;

interface OperationServiceInterface
{
    public function getOperation(
        array $row,
    ): Operation;
}
