<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services;

interface LoggerServiceInterface
{
    public function emergency(string | int $message, array $context = []): void;

    public function alert(string | int $message, array $context = []): void;

    public function critical(string $message, array $context = []): void;

    public function error(string | int $message, array $context = []): void;

    public function warning(string | int $message, array $context = []): void;

    public function notice(string | int $message, array $context = []): void;

    public function info(string | int $message, array $context = []): void;

    public function debug(string | int $message, array $context = []): void;

    public function log(int $level, string | int $message, array $context = []): void;
}
