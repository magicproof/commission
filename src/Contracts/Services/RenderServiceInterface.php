<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services;

interface RenderServiceInterface
{
    public function display(string $view): void;

    public function getDisplay(bool $normalize = false): string;
}
