<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services\CommissionFee\Strategies;

use CommissionTask\Entities\Transaction;

interface StrategyInterface
{
    public function getChargeAmount(
        Transaction $transaction,
    ): string;
}
