<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services\CommissionFee;

use CommissionTask\Entities\Transaction;

interface CommissionFeeInterface
{
    public function calculateCharge(
        Transaction $transaction,
    ): string;
}
