<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services;

interface ApiExchangeRatesInterface
{
    public function getLatest(array $data = []): array;
}
