<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services;

use DateTime;

interface DateServiceInterface
{
    public function getStartOfWeekDate(DateTime $date): DateTime;

    public function getStartOfWeekDateFormatted(DateTime $date): string;
}
