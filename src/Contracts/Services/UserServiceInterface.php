<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services;

use CommissionTask\Entities\User;

interface UserServiceInterface
{
    public function getUser(
        array $row,
    ): User;
}
