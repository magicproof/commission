<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Services;

interface CurrencyServiceInterface
{
    public function getRates(): array;

    public function convertToBaseCurrency(string $amount, string $fromCurrency): string;

    public function convertFromBaseToCurrency(string $amount, string $toCurrency): string;
}
