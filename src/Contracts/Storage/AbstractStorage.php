<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Storage;

use Iterator;
use SplObjectStorage;

abstract class AbstractStorage implements StorageInterface
{
    protected SplObjectStorage $storage;

    public function __construct()
    {
        $this->storage = new SplObjectStorage();
    }

    public function count(): int
    {
        return $this->storage->count();
    }

    public function rewind(): void
    {
        $this->storage->rewind();
    }

    public function getInfo(): mixed
    {
        return $this->storage->getInfo();
    }

    public function attach(object $entity, int | string $key = null): void
    {
        $this->storage->attach($entity, $key);
    }

    public function next(): void
    {
        $this->storage->next();
    }

    public function key(): void
    {
        $this->storage->valid();
    }

    public function valid(): bool
    {
        return $this->storage->valid();
    }

    public function current(): object
    {
        return $this->storage->current();
    }

    public function whereKey(string | int $key): Iterator
    {
        $this->rewind();
        $storage = new SplObjectStorage();
        while ($this->valid()) {
            if ($key === $this->getInfo()) {
                $storage->attach($this->current());
            }
            $this->next();
        }

        return $storage;
    }

    public function find(string | int $key): ?object
    {
        $this->rewind();
        while ($this->valid()) {
            if ($key === $this->getInfo()) {
                return $this->current();
            }
            $this->next();
        }

        return null;
    }
}
