<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Storage;

use Countable;
use Iterator;

interface StorageInterface extends Countable, Iterator
{
    public function whereKey(string | int $key): Iterator;

    public function find(string | int $key): ?object;

    public function attach(object $entity, int | string $key = null): void;
}
