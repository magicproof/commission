<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Validator;

use CommissionTask\Contracts\Config\ConfigInterFace;
use CommissionTask\Contracts\Rules\RuleInterface;
use CommissionTask\Exceptions\ValidationException;

abstract class AbstractValidator implements ValidateInterface
{
    public function __construct(
        protected ConfigInterFace $config
    ) {
    }

    public function validate(array $arguments = []): void
    {
        foreach ($this->getRules() as $argumentKey => $rules) {
            foreach ($rules as $rule) {
                $this->validateWithRule(
                    new $rule($argumentKey, $arguments[$argumentKey] ?? null, $this->config)
            );
            }
        }
    }

    protected function validateWithRule(RuleInterface $rule)
    {
        if (!$rule->passes()) {
            throw new ValidationException($rule->getMessage());
        }
    }

    protected function getRules(): array
    {
        return [];
    }
}
