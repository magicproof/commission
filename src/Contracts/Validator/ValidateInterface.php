<?php

declare(strict_types=1);

namespace CommissionTask\Contracts\Validator;

interface ValidateInterface
{
    public function validate(array $arguments = []): void;
}
