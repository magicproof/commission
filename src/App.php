<?php

declare(strict_types=1);

namespace CommissionTask;

use CommissionTask\Contracts\Services\CommissionFee\CommissionFeeInterface;
use CommissionTask\Contracts\Services\LoggerServiceInterface;
use CommissionTask\Contracts\Services\OperationServiceInterface;
use CommissionTask\Contracts\Services\RenderServiceInterface;
use CommissionTask\Contracts\Services\UserServiceInterface;
use CommissionTask\Entities\Transaction;
use CommissionTask\Exceptions\AppException;
use CommissionTask\Exceptions\DataReadException;
use CommissionTask\Exceptions\ProcessRowException;
use CommissionTask\Readers\ReaderFactoryService;
use Throwable;

class App
{
    public function __construct(
        protected ReaderFactoryService $readerFactoryService,
        protected CommissionFeeInterface $commissionFeeService,
        protected UserServiceInterface $userService,
        protected OperationServiceInterface $operationService,
        protected RenderServiceInterface $renderService,
        protected LoggerServiceInterface $logger,
    ) {
    }

    public function handle(string $path)
    {
        try {
            $rows = $this->readerFactoryService->getReader($path)->getRows();
            foreach ($rows as $row) {
                $this->render($this->processRow($row));
            }
        } catch (DataReadException $dataReadException) {
            $this->log($dataReadException);
            $this->render($dataReadException->getMessage());
        } catch (AppException $appException) {
            $this->log($appException);
            $this->render($appException->getMessage());
        } catch (Throwable $exception) {
            $this->log($exception);
            $this->render('App error. Something went wrong');
        }
    }

    protected function processRow(array $row): string
    {
        try {
            $response = $this->commissionFeeService->calculateCharge(
                new Transaction(
                    $this->operationService->getOperation($row),
                    $this->userService->getUser($row),
                )
            );
        } catch (ProcessRowException $appException) {
            $this->log($appException);
            $response = $appException->getMessage();
        } catch (Throwable $exception) {
            $this->log($exception);
            $response = 'Parse row error. Something went wrong';
        }

        return $response;
    }

    protected function render(string $output): void
    {
        try {
            $this->renderService->display($output);
        } catch (Throwable $exception) {
            $this->log($exception);
        }
    }

    protected function log(Throwable $exception): void
    {
        $this->logger->error($exception->getMessage());
        $this->logger->error($exception->getFile());
        $this->logger->error($exception->getLine());
    }
}
