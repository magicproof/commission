<?php

declare(strict_types=1);

namespace CommissionTask\Entities;

use CommissionTask\Enums\OperationTypeEnum;
use DateTime;

class Operation
{
    public const OPERATION_FIELD_DATE = 'operationDate';
    public const OPERATION_FIELD_TYPE = 'operationType';
    public const OPERATION_FIELD_AMOUNT = 'freeOperationsAmountLimit';
    public const OPERATION_FIELD_CURRENCY = 'operationCurrency';

    private DateTime $date;

    public function __construct(
        string $date,
        private string $type,
        private string $amount,
        private string $currency,
    ) {
        $this->setDate($date);
    }

    public function isDeposit(): bool
    {
        return $this->type === OperationTypeEnum::DEPOSIT;
    }

    public function isWithDraw(): bool
    {
        return $this->type === OperationTypeEnum::WITHDRAW;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(string $date): self
    {
        $this->date = DateTime::createFromFormat('Y-m-d', $date);

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }
}
