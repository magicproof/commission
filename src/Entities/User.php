<?php

declare(strict_types=1);

namespace CommissionTask\Entities;

use CommissionTask\Contracts\Storage\StorageInterface;
use CommissionTask\Enums\UserTypeEnum;

class User
{
    public const FIELD_USER_ID = 'userId';
    public const FIELD_USER_TYPE = 'userType';

    private ?StorageInterface $privateUserOperations = null;

    public function __construct(
        private string $id,
        private string $type,
    ) {
    }

    public function isPrivate(): bool
    {
        return $this->type === UserTypeEnum::PRIVATE;
    }

    public function isBusiness(): bool
    {
        return $this->type === UserTypeEnum::BUSINESS;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getPrivateUserOperations(): ?StorageInterface
    {
        return $this->privateUserOperations;
    }

    public function setPrivateUserOperationStorage(
        StorageInterface $privateUserOperations
    ): StorageInterface {
        return $this->privateUserOperations = $privateUserOperations;
    }
}
