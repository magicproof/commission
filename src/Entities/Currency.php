<?php

declare(strict_types=1);

namespace CommissionTask\Entities;

class Currency
{
    public function __construct(
        private string $name,
        private string $course,
        private bool $isBase,
        private int $precision,
    ) {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCourse(): string
    {
        return $this->course;
    }

    public function setCourse(string $course): void
    {
        $this->course = $course;
    }

    public function isBase(): bool
    {
        return $this->isBase;
    }

    public function setIsBase(bool $isBase): void
    {
        $this->isBase = $isBase;
    }

    public function getPrecision(): int
    {
        return $this->precision;
    }

    public function setPrecision(int $precision): void
    {
        $this->precision = $precision;
    }
}
