<?php

declare(strict_types=1);

namespace CommissionTask\Entities;

class Transaction
{
    public function __construct(
        private Operation $operation,
        private User $user,
    ) {
    }

    public function getOperation(): Operation
    {
        return $this->operation;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function setOperation(Operation $operation): void
    {
        $this->operation = $operation;
    }
}
