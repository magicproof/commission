<?php

declare(strict_types=1);

namespace CommissionTask\Enums;

final class CurrencyTypeEnum
{
    public const EUR = 'EUR';
    public const JPY = 'JPY';
    public const USD = 'USD';
}
