<?php

declare(strict_types=1);

namespace CommissionTask\Enums;

final class OperationTypeEnum
{
    public const DEPOSIT = 'deposit';
    public const WITHDRAW = 'withdraw';
}
