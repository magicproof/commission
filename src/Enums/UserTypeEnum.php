<?php

declare(strict_types=1);

namespace CommissionTask\Enums;

final class UserTypeEnum
{
    public const PRIVATE = 'private';
    public const BUSINESS = 'business';
}
