<?php

declare(strict_types=1);

namespace CommissionTask\Rules;

use CommissionTask\Contracts\Rules\AbstractRule;
use CommissionTask\Contracts\Rules\RuleInterface;

class AllowedUserTypeRule extends AbstractRule implements RuleInterface
{
    public function passes(): bool
    {
        return in_array($this->value, $this->config->get('supported_user_types'), true);
    }

    public function getMessage(): string
    {
        return 'User type '.$this->value.' is not allowed';
    }
}
