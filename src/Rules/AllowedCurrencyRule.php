<?php

declare(strict_types=1);

namespace CommissionTask\Rules;

use CommissionTask\Contracts\Rules\AbstractRule;
use CommissionTask\Contracts\Rules\RuleInterface;

class AllowedCurrencyRule extends AbstractRule implements RuleInterface
{
    public function passes(): bool
    {
        return in_array($this->value, $this->config->get('supported_currency_types'), true);
    }

    public function getMessage(): string
    {
        return 'Currency '.$this->value.' is not allowed';
    }
}
