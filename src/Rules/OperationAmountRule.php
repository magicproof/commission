<?php

declare(strict_types=1);

namespace CommissionTask\Rules;

use CommissionTask\Contracts\Rules\AbstractRule;
use CommissionTask\Contracts\Rules\RuleInterface;

class OperationAmountRule extends AbstractRule implements RuleInterface
{
    public function passes(): bool
    {
        return is_numeric($this->value);
    }

    public function getMessage(): string
    {
        return 'Operation amount is incorrect '.$this->value;
    }
}
