<?php

declare(strict_types=1);

namespace CommissionTask\Rules;

use CommissionTask\Contracts\Rules\AbstractRule;
use CommissionTask\Contracts\Rules\RuleInterface;

class AllowedOperationTypeRule extends AbstractRule implements RuleInterface
{
    public function passes(): bool
    {
        return in_array($this->value, $this->config->get('supported_operation_types'), true);
    }

    public function getMessage(): string
    {
        return 'Operation type '.$this->value.' is not allowed';
    }
}
