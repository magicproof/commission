<?php

declare(strict_types=1);

namespace CommissionTask\Rules;

use CommissionTask\Contracts\Rules\AbstractRule;
use CommissionTask\Contracts\Rules\RuleInterface;
use DateTime;

class DateRule extends AbstractRule implements RuleInterface
{
    public function passes(): bool
    {
        return DateTime::createFromFormat(
                $this->config->get('transaction_operation_date_format'),
                $this->value
            ) !== false;
    }

    public function getMessage(): string
    {
        return 'Date format is incorrect '.$this->value;
    }
}
