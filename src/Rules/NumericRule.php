<?php

declare(strict_types=1);

namespace CommissionTask\Rules;

use CommissionTask\Contracts\Rules\AbstractRule;
use CommissionTask\Contracts\Rules\RuleInterface;

class NumericRule extends AbstractRule implements RuleInterface
{
    public function passes(): bool
    {
        return is_numeric($this->value);
    }

    public function getMessage(): string
    {
        return 'Argument '.$this->key.' is not numeric -'.$this->value;
    }
}
