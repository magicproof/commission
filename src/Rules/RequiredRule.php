<?php

declare(strict_types=1);

namespace CommissionTask\Rules;

use CommissionTask\Contracts\Rules\AbstractRule;
use CommissionTask\Contracts\Rules\RuleInterface;

class RequiredRule extends AbstractRule implements RuleInterface
{
    public function passes(): bool
    {
        return $this->value !== null;
    }

    public function getMessage(): string
    {
        return 'Required argument '.$this->key.' is not passed';
    }
}
