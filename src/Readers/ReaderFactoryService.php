<?php

declare(strict_types=1);

namespace CommissionTask\Readers;

use CommissionTask\Contracts\Readers\ReaderInterface;
use CommissionTask\Exceptions\DataReadException;
use CommissionTask\Exceptions\ExtensionException;
use CommissionTask\Exceptions\FileNotFoundException;
use RuntimeException;
use SplFileObject;
use Throwable;

class ReaderFactoryService
{
    public const EXTENSION_CSV = 'csv';
    protected SplFileObject $file;

    public function __construct(
        protected CsvReader $csvReader,
    ) {
    }

    protected function getAvailableReaders(): array
    {
        return [
            self::EXTENSION_CSV => $this->csvReader,
        ];
    }

    public function getReader(string $filePath): ReaderInterface
    {
        try {
            $availableReaders = array_keys($this->getAvailableReaders());
            $this->file = new SplFileObject($filePath, 'rb');
            if (!in_array($this->file->getExtension(), $availableReaders, true)) {
                throw new ExtensionException(sprintf('Wrong file extension passed - %s. Supported - %s', $this->file->getExtension(), implode(',', $availableReaders)));
            }
        } catch (RuntimeException $exception) {
            throw new FileNotFoundException('No such file or directory - '.$filePath);
        } catch (Throwable $exception) {
            throw new DataReadException('File parse error. Something went wrong');
        }

        return $this->getAvailableReaders()[$this->file->getExtension()]->setFile($this->file);
    }
}
