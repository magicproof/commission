<?php

declare(strict_types=1);

namespace CommissionTask\Readers;

use CommissionTask\Contracts\Readers\ReaderInterface;
use CommissionTask\Exceptions\DataReadException;
use Generator;
use SplFileObject;
use Throwable;

class CsvReader implements ReaderInterface
{
    protected SplFileObject $file;

    public function setFile(SplFileObject $file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getRows(
    ): Generator {
        try {
            while (!$this->file->eof()) {
                yield $this->file->fgetcsv();
            }
        } catch (Throwable $exception) {
            throw new DataReadException('File parse error. Something went wrong');
        }
    }
}
