<?php

declare(strict_types=1);

namespace CommissionTask\Validator;

use CommissionTask\Contracts\Validator\AbstractValidator;
use CommissionTask\Entities\User;
use CommissionTask\Rules\AllowedUserTypeRule;
use CommissionTask\Rules\NumericRule;
use CommissionTask\Rules\RequiredRule;

class UserValidator extends AbstractValidator
{
    public function getRules(): array
    {
        return [
            $this->config->getTransactionFieldOrderKey(User::FIELD_USER_ID) => [
                RequiredRule::class,
                NumericRule::class,
            ],
            $this->config->getTransactionFieldOrderKey(User::FIELD_USER_TYPE) => [
                RequiredRule::class,
                AllowedUserTypeRule::class,
            ],
        ];
    }
}
