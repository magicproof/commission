<?php

declare(strict_types=1);

namespace CommissionTask\Validator;

use CommissionTask\Contracts\Validator\AbstractValidator;
use CommissionTask\Entities\Operation;
use CommissionTask\Rules\AllowedCurrencyRule;
use CommissionTask\Rules\AllowedOperationTypeRule;
use CommissionTask\Rules\DateRule;
use CommissionTask\Rules\OperationAmountRule;
use CommissionTask\Rules\RequiredRule;

class OperationValidator extends AbstractValidator
{
    public function getRules(): array
    {
        return [
            $this->config->getTransactionFieldOrderKey(Operation::OPERATION_FIELD_DATE) => [
                RequiredRule::class,
                DateRule::class,
            ],
            $this->config->getTransactionFieldOrderKey(Operation::OPERATION_FIELD_CURRENCY) => [
                RequiredRule::class,
                AllowedCurrencyRule::class,
            ],
            $this->config->getTransactionFieldOrderKey(Operation::OPERATION_FIELD_AMOUNT) => [
                RequiredRule::class,
                OperationAmountRule::class,
            ],
            $this->config->getTransactionFieldOrderKey(Operation::OPERATION_FIELD_TYPE) => [
                RequiredRule::class,
                AllowedOperationTypeRule::class,
            ],
        ];
    }
}
