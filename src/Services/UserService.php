<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\UserServiceInterface;
use CommissionTask\Contracts\Storage\StorageInterface;
use CommissionTask\Contracts\Validator\ValidateInterface;
use CommissionTask\Entities\User;

class UserService implements UserServiceInterface
{
    public function __construct(
        protected ValidateInterface $validator,
        protected array $transactionFieldOrder,
        protected StorageInterface $userStorage
    ) {
    }

    public function getUser(array $row): User
    {
        $this->validator->validate($row);
        $userId = $row[$this->transactionFieldOrder[User::FIELD_USER_ID]];
        $user = $this->userStorage->find($userId);
        if (!$user) {
            $user = new User(
                $userId,
                $row[$this->transactionFieldOrder[User::FIELD_USER_TYPE]]
            );
            $this->userStorage->attach($user, $userId);
        }

        return $user;
    }
}
