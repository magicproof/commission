<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\LoggerServiceInterface;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Psr\Log\LoggerInterface;

class LoggerService implements LoggerServiceInterface
{
    protected ?string $logPath;

    public function __construct(
        protected LoggerInterface $logger,
        array $config,
    ) {
        $channel = $config['channel'] ?? '';
        $this->logPath = $config[$channel]['path'] ?? null;
        $logLevel = $config[$channel]['level'] ?? Logger::DEBUG;
        $this->logger->pushHandler(new StreamHandler($this->logPath, $logLevel));
    }

    public function emergency(string | int $message, array $context = []): void
    {
        $this->logger->emergency($message, $context);
    }

    public function alert(string | int $message, array $context = []): void
    {
        $this->logger->alert($message, $context);
    }

    public function critical(string | int $message, array $context = []): void
    {
        $this->logger->critical($message, $context);
    }

    public function error(string | int $message, array $context = []): void
    {
        $this->logger->error($message, $context);
    }

    public function warning(string | int $message, array $context = []): void
    {
        $this->logger->warning($message, $context);
    }

    public function notice(string | int $message, array $context = []): void
    {
        $this->logger->notice($message, $context);
    }

    public function info(string | int $message, array $context = []): void
    {
        $this->logger->info($message, $context);
    }

    public function debug(string | int $message, array $context = []): void
    {
        $this->logger->debug($message, $context);
    }

    public function log(int $level, string | int $message, array $context = []): void
    {
        $this->logger->log($level, $message, $context);
    }

    public function getLogPath(): ?string
    {
        return $this->logPath;
    }
}
