<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\CurrencyServiceInterface;
use CommissionTask\Contracts\Services\DateServiceInterface;
use CommissionTask\Contracts\Storage\StorageInterface;
use CommissionTask\Entities\Operation;
use CommissionTask\Entities\Transaction;
use CommissionTask\Storage\Storage;

class WithDrawPrivateService
{
    public const BASE_AMOUNT = '0';

    protected StorageInterface $userOperationsStorage;

    public function __construct(
        protected DateServiceInterface $dateService,
        protected MathService $mathService,
        protected CurrencyServiceInterface $currencyService,
        protected string $freeOperationsAmountLimit,
        protected int $freeOperationsCountLimit,
    ) {
    }

    public function getAmountToCharge(
        Transaction $transaction,
    ): string {
        $user = $transaction->getUser();
        $operation = $transaction->getOperation();
        $this->userOperationsStorage = $user->getPrivateUserOperations()
            ?: $user->setPrivateUserOperationStorage(
                new Storage()
            );

        if ($this->canAttach($operation)) {
            $this->userOperationsStorage->attach($operation, $this->dateService->getStartOfWeekDateFormatted($operation->getDate()));
            $amountToCharge = $this->currencyService->convertFromBaseToCurrency(
                $this->getAmountByOperationDate($operation),
                $operation->getCurrency()
            );
        } else {
            $amountToCharge = $transaction->getOperation()->getAmount();
        }

        return $amountToCharge;
    }

    protected function canAttach(Operation $operation): bool
    {
        return !$this->isFreeTransactionsLimitExceededByOperationDate($operation)
               && !$this->isExceededAmountByOperationDate($operation);
    }

    protected function getAmountByOperationDate(Operation $operation): string
    {
        $exceededAmount = $this->getExceededAmountByOperationDate($operation);

        return $exceededAmount > 0 ? $exceededAmount : self::BASE_AMOUNT;
    }

    protected function getExceededAmountByOperationDate(Operation $operation): string
    {
        return $this->mathService->sub(
            $this->getSumAmountByOperationDate($operation),
            $this->freeOperationsAmountLimit,
        );
    }

    protected function isExceededAmountByOperationDate(Operation $operation): bool
    {
        return $this->getExceededAmountByOperationDate($operation) > 0;
    }

    protected function isFreeTransactionsLimitExceededByOperationDate(Operation $operation): bool
    {
        return $this->userOperationsStorage
                   ->whereKey($this->dateService->getStartOfWeekDateFormatted($operation->getDate()))
                   ->count() >= $this->freeOperationsCountLimit;
    }

    protected function getSumAmountByOperationDate(Operation $currentOperation): string
    {
        $operations = $this->userOperationsStorage->whereKey(
            $this->dateService->getStartOfWeekDateFormatted($currentOperation->getDate())
        );
        $sum = self::BASE_AMOUNT;
        foreach ($operations as $operation) {
            $sum = $this->mathService
                ->add(
                    $sum,
                    $this->currencyService->convertToBaseCurrency(
                        $operation->getAmount(),
                        $operation->getCurrency()
                    ),
                );
        }

        return $sum;
    }
}
