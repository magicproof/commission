<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\ApiExchangeRatesInterface;
use CommissionTask\Contracts\Services\CurrencyServiceInterface;
use CommissionTask\Contracts\Services\MathInterface;
use CommissionTask\Contracts\Storage\StorageInterface;
use CommissionTask\Entities\Currency;
use CommissionTask\Exceptions\ProcessRowException;
use Psr\Http\Client\ClientExceptionInterface;

class CurrencyService implements CurrencyServiceInterface
{
    public function __construct(
        protected array $supportedCurrencyTypes,
        protected array $supportedCurrencyPrecisions,
        protected string $baseCurrency,
        protected StorageInterface $storage,
        protected MathInterface $math,
        protected ApiExchangeRatesInterface $apiExchangeRatesService,
    ) {
        $rates = $this->getRates();
        foreach ($this->supportedCurrencyTypes as $currencyName) {
            $this->storage->attach(
                new Currency(
                    $currencyName,
                    (string) $rates[$currencyName],
                    $this->baseCurrency === $currencyName,
                    $this->supportedCurrencyPrecisions[$currencyName]
                ),
                $currencyName
            );
        }
    }

    public function getRates(): array
    {
        try {
            return $this->apiExchangeRatesService->getLatest();
        } catch (ClientExceptionInterface $exception) {
            throw new ProcessRowException($exception->getMessage());
        }
    }

    public function convertToBaseCurrency(string $amount, string $fromCurrency): string
    {
        $currency = $this->getCurrency($fromCurrency);

        return $this->math->div($amount, $currency->getCourse());
    }

    protected function getCurrency(string $code): Currency
    {
        return $this->storage->find($code);
    }

    public function convertFromBaseToCurrency(string $amount, string $toCurrency): string
    {
        $currency = $this->getCurrency($toCurrency);

        return $this->math->mul($amount, $currency->getCourse());
    }
}
