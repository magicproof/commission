<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\DateServiceInterface;
use DateTime;

class DateService implements DateServiceInterface
{
    public function getStartOfWeekDate(DateTime $date): DateTime
    {
        $date = clone $date;
        $date->setTime(0, 0, 0);
        if ($this->isMonday($date)) {
            return $date;
        } else {
            return $date->modify('last monday');
        }
    }

    public function getStartOfWeekDateFormatted(DateTime $date): string
    {
        return $this->getStartOfWeekDate($date)->format('Y-m-d');
    }

    protected function isMonday(DateTime $date): bool
    {
        return $date->format('N') === '1';
    }
}
