<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\RenderServiceInterface;
use Symfony\Component\Console\Output\OutputInterface;

class RenderService implements RenderServiceInterface
{
    public function __construct(
        protected OutputInterface $output
    ) {
    }

    public function display(string $view): void
    {
        $this->output->writeln($view);
    }

    public function getDisplay(bool $normalize = false): string
    {
        $display = $this->output->fetch();
        if ($normalize) {
            $display = str_replace(\PHP_EOL, "\n", $display);
        }

        return $display;
    }
}
