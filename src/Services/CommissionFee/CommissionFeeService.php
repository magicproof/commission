<?php

declare(strict_types=1);

namespace CommissionTask\Services\CommissionFee;

use CommissionTask\Contracts\Services\CommissionFee\CommissionFeeInterface;
use CommissionTask\Contracts\Services\CommissionFee\Strategies\StrategyInterface;
use CommissionTask\Entities\Transaction;
use CommissionTask\Exceptions\ProcessRowException;
use CommissionTask\Services\MathService;

class CommissionFeeService implements CommissionFeeInterface
{
    public const DEFAULT_PRECISION = 2;
    public const PERCENT_COEFFICIENT = '100';

    protected ?string $commissionFeeCoefficient;

    protected ?StrategyInterface $chargeStrategy;

    public function __construct(
        protected MathService $math,
        protected StrategyResolverService $strategyResolverService,
        protected CommissionFeeResolverService $commissionFeeResolverService,
        protected array $supportedCurrencyPrecisions,
        protected StrategyFactoryService $strategyFactoryService,
    ) {
    }

    public function calculateCharge(
        Transaction $transaction,
    ): string
    {
        $strategy = $this->chargeStrategy ?? $this->getOperationStrategy($transaction);

        return $this->math->formatOutput(
            $this->math->div(
                $this->math->mul(
                    $strategy->getChargeAmount($transaction),
                    $this->getCommissionFeeCoefficient($transaction),
                ),
                self::PERCENT_COEFFICIENT,
            ),
            $this->getPrecision($transaction)
        );
    }

    protected function getOperationStrategy(Transaction $transaction): StrategyInterface
    {
        $strategyClass = $this->strategyResolverService->getClass($transaction);
        $strategy = $this->strategyFactoryService->getStrategy($strategyClass);
        if ($strategyClass === null) {
            throw new ProcessRowException('Unsupported commission strategy in resolvers');
        }
        if ($strategy === null) {
            throw new ProcessRowException('Unsupported commission strategy in StrategyFactoryService');
        }

        return $strategy;
    }

    protected function getCommissionFeeCoefficient(Transaction $transaction): string
    {
        $this->commissionFeeCoefficient = $this->commissionFeeResolverService->getFeeCoefficient($transaction);
        if ($this->commissionFeeCoefficient === null) {
            throw new ProcessRowException('Unsupported commission fee coefficient in resolver');
        }

        return $this->commissionFeeCoefficient;
    }

    protected function getPrecision(Transaction $transaction): int
    {
        return $this->supportedCurrencyPrecisions[$transaction->getOperation()->getCurrency()]
               ?? self::DEFAULT_PRECISION;
    }

    public function setChargeStrategy(?StrategyInterface $chargeStrategy): self
    {
        $this->chargeStrategy = $chargeStrategy;

        return $this;
    }

}
