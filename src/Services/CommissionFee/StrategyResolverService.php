<?php

declare(strict_types=1);

namespace CommissionTask\Services\CommissionFee;

use CommissionTask\Entities\Transaction;
use CommissionTask\Services\CommissionFee\Strategies\DefaultStrategy;
use CommissionTask\Services\CommissionFee\Strategies\WithdrawPrivateStrategy;

class StrategyResolverService
{
    public function getClass(Transaction $transaction): ?string
    {
        $operation = $transaction->getOperation();
        $user = $transaction->getUser();
        $class = null;
        if ($operation->isDeposit()
            || ($operation->isWithDraw() && $user->isBusiness())
        ) {
            $class = DefaultStrategy::class;
        } elseif ($operation->isWithDraw() && $user->isPrivate()) {
            $class = WithdrawPrivateStrategy::class;
        }

        return $class;
    }
}
