<?php

declare(strict_types=1);

namespace CommissionTask\Services\CommissionFee;

use CommissionTask\Contracts\Services\CommissionFee\Strategies\StrategyInterface;
use CommissionTask\Services\CommissionFee\Strategies\DefaultStrategy;
use CommissionTask\Services\CommissionFee\Strategies\WithdrawPrivateStrategy;

class StrategyFactoryService
{
    public function __construct(
        protected DefaultStrategy $defaultStrategy,
        protected WithdrawPrivateStrategy $withdrawPrivateStrategy,
    ) {
    }

    protected function getAvailableStrategies(): array
    {
        return [
            DefaultStrategy::class => $this->defaultStrategy,
            WithdrawPrivateStrategy::class => $this->withdrawPrivateStrategy,
        ];
    }

    public function getStrategy(string $type): ?StrategyInterface
    {
        return $this->getAvailableStrategies()[$type] ?? null;
    }
}
