<?php

declare(strict_types=1);

namespace CommissionTask\Services\CommissionFee\Strategies;

use CommissionTask\Contracts\Services\CommissionFee\Strategies\StrategyInterface;
use CommissionTask\Entities\Transaction;

class DefaultStrategy implements StrategyInterface
{
    public function getChargeAmount(
        Transaction $transaction,
    ): string
    {
        return $transaction->getOperation()->getAmount();
    }
}
