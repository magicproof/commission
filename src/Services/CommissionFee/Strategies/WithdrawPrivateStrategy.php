<?php

declare(strict_types=1);

namespace CommissionTask\Services\CommissionFee\Strategies;

use CommissionTask\Contracts\Services\CommissionFee\Strategies\StrategyInterface;
use CommissionTask\Entities\Transaction;
use CommissionTask\Services\WithDrawPrivateService;

class WithdrawPrivateStrategy implements StrategyInterface
{
    public function __construct(
        protected WithDrawPrivateService $withDrawPrivateService
    ) {
    }

    public function getChargeAmount(
        Transaction $transaction,
    ): string {
        return $this->withDrawPrivateService->getAmountToCharge($transaction);
    }
}
