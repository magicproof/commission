<?php

declare(strict_types=1);

namespace CommissionTask\Services\CommissionFee;

use CommissionTask\Entities\Transaction;

class CommissionFeeResolverService
{
    public const DEPOSIT_TYPE = 1;
    public const WITHDRAW_PRIVATE_TYPE = 2;
    public const WITHDRAW_BUSINESS_TYPE = 3;

    public function __construct(
        protected array $supportedCommissionFeeCoefficients,
    ) {
    }

    public function getFeeCoefficient(Transaction $transaction): ?string
    {
        $operation = $transaction->getOperation();
        $user = $transaction->getUser();
        $feeType = null;
        if ($operation->isDeposit()) {
            $feeType = self::DEPOSIT_TYPE;
        } elseif ($operation->isWithDraw()) {
            if ($user->isPrivate()) {
                $feeType = self::WITHDRAW_PRIVATE_TYPE;
            } elseif ($user->isBusiness()) {
                $feeType = self::WITHDRAW_BUSINESS_TYPE;
            }
        }

        return $this->supportedCommissionFeeCoefficients[$feeType] ?? null;
    }
}
