<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\ApiExchangeRatesInterface;
use CommissionTask\Exceptions\HttpRequestException;
use GuzzleHttp\Client;

class ApiExchangeRatesService implements ApiExchangeRatesInterface
{
    protected Client $client;

    public function __construct(
        string $apiUrl,
        protected string $accessKey,
        protected string $baseCurrency,
        protected array $supportedCurrencyTypes,
    ) {
        $this->client = new Client(['base_uri' => $apiUrl]);
    }

    public function getLatest(array $data = []): array
    {
        $response = $this->client->request(
            'GET',
            'latest',
            [
                'query' => [
                    'access_key' => $this->accessKey,
                    'base' => $this->baseCurrency,
                    'symbols' => implode(',', $this->supportedCurrencyTypes),
                ],
            ]
        );
        $data = json_decode($response->getBody()->getContents(), true);
        if (empty($data['success']) || empty($data['rates'])) {
            throw new HttpRequestException('Api rates are not fetched');
        }

        return $data['rates'];
    }
}
