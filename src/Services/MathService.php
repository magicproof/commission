<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\MathInterface;

class MathService implements MathInterface
{
    public const ZERO = '0';
    public const POINT = '.';
    public const ROUND_VALUE = '9';
    public const MINUS = '-';

    public function __construct(
        private int $scale
    ) {
    }

    public function formatOutput(string $number, int $outputPrecision): string
    {
        $formatValue = $this->getFormatValue($this->getDecimal($number), $outputPrecision);

        return str_contains($number, self::MINUS)
            ? bcsub($number, $formatValue, $outputPrecision)
            : bcadd($number, $formatValue, $outputPrecision);
    }

    private function getDecimal(string $number): string
    {
        $decimal = '';
        if(str_contains($number, self::POINT)){
            [, $decimal] = explode(self::POINT, $number);
        }
        return $decimal;
    }

    private function getFormatValue(string $decimal, int $outputPrecision): string
    {
        if (
            strlen($decimal) > $outputPrecision
            && (int)$decimal[$outputPrecision] > 0
        ) {
            return self::ZERO
                   .self::POINT
                   .str_repeat(self::ZERO, $outputPrecision)
                   .self::ROUND_VALUE;
        }

        return self::ZERO;
    }

    public function add(string $leftOperand, string $rightOperand): string
    {
        return bcadd($leftOperand, $rightOperand, $this->scale);
    }

    public function mul(string $leftOperand, string $rightOperand): string
    {
        return bcmul($leftOperand, $rightOperand, $this->scale);
    }

    public function div(string $leftOperand, string $rightOperand): string
    {
        return bcdiv($leftOperand, $rightOperand, $this->scale);
    }

    public function sub(string $leftOperand, string $rightOperand): string
    {
        return bcsub($leftOperand, $rightOperand, $this->scale);
    }

    public function setScale(int $scale): void
    {
        $this->scale = $scale;
    }
}
