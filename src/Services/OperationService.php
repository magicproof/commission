<?php

declare(strict_types=1);

namespace CommissionTask\Services;

use CommissionTask\Contracts\Services\OperationServiceInterface;
use CommissionTask\Contracts\Validator\ValidateInterface;
use CommissionTask\Entities\Operation;

class OperationService implements OperationServiceInterface
{
    public function __construct(
        protected ValidateInterface $validator,
        protected array $transactionFieldOrder,
    ) {
    }

    public function getOperation(array $row): Operation
    {
        $this->validator->validate($row);

        return new Operation(
            $row[$this->transactionFieldOrder[Operation::OPERATION_FIELD_DATE]],
            $row[$this->transactionFieldOrder[Operation::OPERATION_FIELD_TYPE]],
            $row[$this->transactionFieldOrder[Operation::OPERATION_FIELD_AMOUNT]],
            $row[$this->transactionFieldOrder[Operation::OPERATION_FIELD_CURRENCY]],
        );
    }
}
