<?php

declare(strict_types=1);

use CommissionTask\App;
use CommissionTask\Config\Config;
use CommissionTask\Contracts\Config\ConfigInterFace;
use CommissionTask\Contracts\Services\ApiExchangeRatesInterface;
use CommissionTask\Contracts\Services\CommissionFee\CommissionFeeInterface;
use CommissionTask\Contracts\Services\CurrencyServiceInterface;
use CommissionTask\Contracts\Services\DateServiceInterface;
use CommissionTask\Contracts\Services\LoggerServiceInterface;
use CommissionTask\Contracts\Services\MathInterface;
use CommissionTask\Contracts\Services\OperationServiceInterface;
use CommissionTask\Contracts\Services\RenderServiceInterface;
use CommissionTask\Contracts\Services\UserServiceInterface;
use CommissionTask\Contracts\Storage\StorageInterface;
use CommissionTask\Readers\CsvReader;
use CommissionTask\Readers\ReaderFactoryService;
use CommissionTask\Services\ApiExchangeRatesService;
use CommissionTask\Services\CommissionFee\CommissionFeeResolverService;
use CommissionTask\Services\CommissionFee\CommissionFeeService;
use CommissionTask\Services\CommissionFee\Strategies\DefaultStrategy;
use CommissionTask\Services\CommissionFee\Strategies\WithdrawPrivateStrategy;
use CommissionTask\Services\CommissionFee\StrategyFactoryService;
use CommissionTask\Services\CommissionFee\StrategyResolverService;
use CommissionTask\Services\CurrencyService;
use CommissionTask\Services\DateService;
use CommissionTask\Services\LoggerService;
use CommissionTask\Services\MathService;
use CommissionTask\Services\OperationService;
use CommissionTask\Services\RenderService;
use CommissionTask\Services\UserService;
use CommissionTask\Services\WithDrawPrivateService;
use CommissionTask\Storage\Storage;
use CommissionTask\Validator\OperationValidator;
use CommissionTask\Validator\UserValidator;
use Monolog\Logger;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Output\BufferedOutput;
use Symfony\Component\Console\Output\OutputInterface;

return [
    App::class => function (ContainerInterface $c) {
        return new App(
            $c->get(ReaderFactoryService::class),
            $c->get(CommissionFeeInterface::class),
            $c->get(UserServiceInterface::class),
            $c->get(OperationServiceInterface::class),
            $c->get(RenderServiceInterface::class),
            $c->get(LoggerServiceInterface::class),
        );
    },
    UserValidator::class => function (ContainerInterface $c) {
        return new UserValidator(
            $c->get(ConfigInterFace::class)
        );
    },
    OperationValidator::class => function (ContainerInterface $c) {
        return new OperationValidator(
            $c->get(ConfigInterFace::class)
        );
    },
    LoggerServiceInterface::class => function (ContainerInterface $c) {
        return new LoggerService(
            $c->get(LoggerInterface::class),
            $c->get(ConfigInterFace::class)->get('logging'),
        );
    },
    ConfigInterFace::class => function (ContainerInterface $c) {
        return new Config(require 'config/app.php');
    },
    WithdrawPrivateStrategy::class => function (ContainerInterface $c) {
        return new WithdrawPrivateStrategy(
            $c->get(WithDrawPrivateService::class),
        );
    },
    DefaultStrategy::class => function (ContainerInterface $c) {
        return new DefaultStrategy(
        );
    },
    CommissionFeeInterface::class => function (ContainerInterface $c) {
        return new CommissionFeeService(
            $c->get(MathInterface::class),
            $c->get(StrategyResolverService::class),
            $c->get(CommissionFeeResolverService::class),
            $c->get(ConfigInterFace::class)->get('supported_currency_precisions'),
            $c->get(StrategyFactoryService::class),
        );
    },
    StorageInterface::class => function (ContainerInterface $c) {
        return new Storage();
    },
    MathInterface::class => function (ContainerInterface $c) {
        return new MathService($c->get(ConfigInterFace::class)->get('math_scale'));
    },
    WithDrawPrivateService::class => function (ContainerInterface $c) {
        return new WithDrawPrivateService(
            $c->get(DateServiceInterface::class),
            $c->get(MathInterface::class),
            $c->get(CurrencyServiceInterface::class),
            $c->get(ConfigInterFace::class)->get('free_transaction_limits.withdraw_private.operations_amount'),
            $c->get(ConfigInterFace::class)->get('free_transaction_limits.withdraw_private.operations_count'),
        );
    },
    CurrencyServiceInterface::class => function (ContainerInterface $c) {
        return new CurrencyService(
            $c->get(ConfigInterFace::class)->get('supported_currency_types'),
            $c->get(ConfigInterFace::class)->get('supported_currency_precisions'),
            $c->get(ConfigInterFace::class)->get('base_currency'),
            new Storage(),
            $c->get(MathInterface::class),
            $c->get(ApiExchangeRatesInterface::class),
        );
    },
    ApiExchangeRatesInterface::class => function (ContainerInterface $c) {
        return new ApiExchangeRatesService(
            $c->get(ConfigInterFace::class)->get('exchangeratesapi.url'),
            $c->get(ConfigInterFace::class)->get('exchangeratesapi.access_key'),
            $c->get(ConfigInterFace::class)->get('base_currency'),
            $c->get(ConfigInterFace::class)->get('supported_currency_types'),
        );
    },
    UserServiceInterface::class => function (ContainerInterface $c) {
        return new UserService(
            $c->get(UserValidator::class),
            $c->get(ConfigInterFace::class)->get('transaction_field_order'),
            new Storage(),
        );
    },
    OperationServiceInterface::class => function (ContainerInterface $c) {
        return new OperationService(
            $c->get(OperationValidator::class),
            $c->get(ConfigInterFace::class)->get('transaction_field_order'),
        );
    },
    RenderServiceInterface::class => function (ContainerInterface $c) {
        return new RenderService(
            $c->get(OutputInterface::class),
        );
    },
    LoggerInterface::class => function (ContainerInterface $c) {
        return new Logger(
            $c->get(ConfigInterFace::class)->get('logging.channel')
        );
    },
    OutputInterface::class => function (ContainerInterface $c) {
        return new BufferedOutput();
    },
    DateServiceInterface::class => function (ContainerInterface $c) {
        return new DateService();
    },
    CsvReader::class => function (ContainerInterface $c) {
        return new CsvReader();
    },
    ReaderFactoryService::class => function (ContainerInterface $c) {
        return new ReaderFactoryService(
            $c->get(CsvReader::class),
        );
    },
    StrategyResolverService::class => function (ContainerInterface $c) {
        return new StrategyResolverService();
    },
    CommissionFeeResolverService::class => function (ContainerInterface $c) {
        return new CommissionFeeResolverService(
            $c->get(ConfigInterFace::class)->get('supported_commission_fee_coefficients'),
        );
    },
];
