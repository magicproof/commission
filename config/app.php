<?php

declare(strict_types=1);

use CommissionTask\Entities\Operation;
use CommissionTask\Entities\User;
use CommissionTask\Enums\CurrencyTypeEnum;
use CommissionTask\Enums\OperationTypeEnum;
use CommissionTask\Enums\UserTypeEnum;
use CommissionTask\Services\CommissionFee\CommissionFeeResolverService;
use Monolog\Logger;

return [
    'logging' => [
        'channel' => 'default',
        'default' => [
            'path' => 'storage/logs/app.logs',
            'level' => Logger::WARNING,
        ],
    ],
    'transaction_operation_date_format' => 'Y-m-d',
    'transaction_field_order' => [
        Operation::OPERATION_FIELD_DATE => 0,
        User::FIELD_USER_ID => 1,
        User::FIELD_USER_TYPE => 2,
        Operation::OPERATION_FIELD_TYPE => 3,
        Operation::OPERATION_FIELD_AMOUNT => 4,
        Operation::OPERATION_FIELD_CURRENCY => 5,
    ],
    'supported_operation_types' => [
        OperationTypeEnum::DEPOSIT,
        OperationTypeEnum::WITHDRAW,
    ],
    'supported_user_types' => [
        UserTypeEnum::BUSINESS,
        UserTypeEnum::PRIVATE,
    ],
    'supported_currency_types' => [
        CurrencyTypeEnum::EUR,
        CurrencyTypeEnum::USD,
        CurrencyTypeEnum::JPY,
    ],
    'supported_currency_precisions' => [
        CurrencyTypeEnum::EUR => 2,
        CurrencyTypeEnum::USD => 2,
        CurrencyTypeEnum::JPY => 0,
    ],
    'free_transaction_limits' => [
        'withdraw_private' => [
            'operations_amount' => '1000',
            'operations_count' => 3,
        ],
    ],
    'exchangeratesapi' => [
        'access_key' => 'a2c3365b19ed3700d42491d8776dbfb8',
        'url' => 'http://api.exchangeratesapi.io/v1/',
    ],
    'base_currency' => CurrencyTypeEnum::EUR,
    'supported_commission_fee_coefficients' => [
            CommissionFeeResolverService::DEPOSIT_TYPE => '0.03',
            CommissionFeeResolverService::WITHDRAW_PRIVATE_TYPE => '0.3',
            CommissionFeeResolverService::WITHDRAW_BUSINESS_TYPE => '0.5',
        ],
    'math_scale' => 5,
];
