## Commission

## Requirements

- Installed [Docker](https://docs.docker.com/install/)
- Installed [Docker-compose](https://docs.docker.com/compose/install/)

### Installing

1. Clone repository
   ```git clone git@gitlab.com:magicproof/commission.git YOUR_PROJECT_FOLDER```
2. Change dir to YOUR_PROJECT_FOLDER
3. Run `USER_ID="$(id -u)" GROUP_ID="$(id -g)" docker-compose up -d` to up app in docker

### Usage

- Run `USER_ID="$(id -u)" GROUP_ID="$(id -g)" docker-compose exec php sh`
- Run `php scripts/index.php calculate data/input.csv` in YOUR_PROJECT_FOLDER to start the application
- Run `./bin/phpunit` in YOUR_PROJECT_FOLDER to run tests off the application