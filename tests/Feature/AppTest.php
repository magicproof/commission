<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Feature;

use CommissionTask\Commands\CalculateCommission;
use CommissionTask\Contracts\Readers\ReaderInterface;
use CommissionTask\Readers\CsvReader;
use CommissionTask\Readers\ReaderFactoryService;
use CommissionTask\Tests\BasePhpUnit;
use Generator;
use Symfony\Component\Console\Tester\CommandTester;

class AppTest extends BasePhpUnit
{
    protected ReaderInterface      $reader;
    protected ReaderFactoryService $readerFactoryService;
    protected CommandTester        $commandTester;

    public function setUp(): void
    {
        parent::setUp();
        $this->reader = $this->createStub(CsvReader::class);
        $this->readerFactoryService = $this->createStub(ReaderFactoryService::class);
        $this->readerFactoryService->method('getReader')
            ->willReturn($this->reader);

        $this->container->set(readerFactoryService::class, $this->readerFactoryService);
        $this->container->set(ReaderInterface::class, $this->reader);
        $this->mockRateService();
        $this->commandTester = new CommandTester(new CalculateCommission());
    }

    public function testComplex()
    {
        $this->reader
            ->method('getRows')
            ->willReturn($this->getGenerator());

        $this->commandTester->execute(['filepath' => './data/input.csv']);

        $this->assertEquals(
            $this->getExpected(),
            $this->commandTester->getDisplay()
        );
    }

    public function testWeekRule()
    {
        $this->reader
            ->method('getRows')
            ->willReturn($this->getGeneratorForWeekRuleTest());

        $this->commandTester->execute(['filepath' => './data/input.csv']);

        $this->assertEquals(
            $this->getExpectedForWeekRuleTest(),
            $this->commandTester->getDisplay()
        );
    }

    protected function getGenerator(): Generator
    {
        $rows = [
            ['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'EUR'],
            ['2015-01-01', '4', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-01-05', '4', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-01-05', '1', 'private', 'deposit', '200.00', 'EUR'],
            ['2016-01-06', '2', 'business', 'withdraw', '300.00', 'EUR'],
            ['2016-01-06', '1', 'private', 'withdraw', '30000', 'JPY'],
            ['2016-01-07', '1', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-01-07', '1', 'private', 'withdraw', '100.00', 'USD'],
            ['2016-01-10', '1', 'private', 'withdraw', '100.00', 'EUR'],
            ['2016-01-10', '2', 'business', 'deposit', '10000.00', 'EUR'],
            ['2016-01-10', '3', 'private', 'withdraw', '1000.00', 'EUR'],
            ['2016-02-15', '1', 'private', 'withdraw', '300.00', 'EUR'],
            ['2016-02-19', '5', 'private', 'withdraw', '3000000', 'JPY'],
        ];
        foreach ($rows as $row) {
            yield $row;
        }
    }

    protected function getGeneratorForWeekRuleTest(): Generator
    {
        $rows = [
            'private withdraw user 1 week 1 test 1' => ['2014-12-25', '1', 'private', 'withdraw', '30000', 'JPY'],
            'private withdraw user 1 week 1 test 2' => ['2014-12-26', '1', 'private', 'withdraw', '200.00', 'EUR'],
            'private withdraw user 1 week 1 test 3' => ['2014-12-27', '1', 'private', 'withdraw', '500.00', 'USD'],
            'private withdraw user 1 week 1 test 4' => ['2014-12-27', '1', 'private', 'withdraw', '1000.00', 'USD'],
            'private withdraw user 1 week 1 test 5' => ['2014-12-28', '1', 'private', 'withdraw', '1000.00', 'USD'],
            'private withdraw user 2 week 1 test 6' => ['2014-12-29', '2', 'private', 'withdraw', '300000', 'JPY'],
            'private withdraw user 1 week 2 test 7' => ['2014-12-29', '1', 'private', 'withdraw', '1500.00', 'USD'],
        ];
        foreach ($rows as $row) {
            yield $row;
        }
    }

    protected function getExpectedForWeekRuleTest(): string
    {
        return implode(
                   PHP_EOL,
                   [
                       '0',
                       '0.00',
                       '0.00',
                       '3.00',
                       '3.00',
                       '512',
                       '1.05',
                   ]
               ).PHP_EOL;
    }

    protected function getExpected(): string
    {
        return implode(
                   PHP_EOL,
                   [
                       '0.60',
                       '3.00',
                       '0.00',
                       '0.06',
                       '1.50',
                       '0',
                       '0.70',
                       '0.30',
                       '0.30',
                       '3.00',
                       '0.00',
                       '0.00',
                       '8612',
                   ]
               ).PHP_EOL;
    }
}
