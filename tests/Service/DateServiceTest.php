<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Services\DateServiceInterface;
use CommissionTask\Tests\BasePhpUnit;
use DateTime;

class DateServiceTest extends BasePhpUnit
{
    private DateServiceInterface $dateService;

    public function setUp(): void
    {
        parent::setUp();
        $this->dateService = $this->container->get(DateServiceInterface::class);
    }

    /**
     * @dataProvider dataProviderDateFormat
     */
    public function testDateFormat(string $date, string $expected)
    {
        $this->assertEquals(
            $expected,
            $this->dateService->getStartOfWeekDateFormatted(DateTime::createFromFormat('Y-m-d', $date))
        );
    }

    public function dataProviderDateFormat(): array
    {
        return [
            'test 1' => ['2014-12-25', '2014-12-22'],
            'test 2' => ['2014-12-28', '2014-12-22'],
            'test 3' => ['2014-12-29', '2014-12-29'],
        ];
    }
}
