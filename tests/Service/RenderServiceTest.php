<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Services\RenderServiceInterface;
use CommissionTask\Tests\BasePhpUnit;

class RenderServiceTest extends BasePhpUnit
{
    private RenderServiceInterface $renderService;

    public function setUp(): void
    {
        parent::setUp();
        $this->renderService = $this->container->get(RenderServiceInterface::class);
    }

    /**
     * @dataProvider dataProviderRender
     */
    public function testRender(
        string $toRender,
        string $expected,
    ) {
        $this->renderService->display(
            $toRender
        );
        $this->assertEquals($this->renderService->getDisplay(), $expected);
    }

    public function dataProviderRender(): array
    {
        return [
            'test 1' => ['test render', 'test render'.PHP_EOL],
        ];
    }
}
