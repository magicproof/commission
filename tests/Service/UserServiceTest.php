<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Services\UserServiceInterface;
use CommissionTask\Entities\User;
use CommissionTask\Exceptions\ValidationException;
use CommissionTask\Tests\BasePhpUnit;

class UserServiceTest extends BasePhpUnit
{
    private UserServiceInterface $userService;

    public function setUp(): void
    {
        parent::setUp();
        $this->userService = $this->container->get(UserServiceInterface::class);
    }

    /**
     * @dataProvider dataProviderGetUser
     */
    public function testGetUser(array $row)
    {
        $this->assertInstanceOf(User::class, $this->userService->getUser($row));
    }

    public function testNotValidaData()
    {
        $this->expectException(
            ValidationException::class,
        );
        $this->userService->getUser(['4', '1200.00']);
    }

    public function dataProviderGetUser(): array
    {
        return [
            'test 1' => [['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'EUR']],
            'test 2' => [['2015-01-01', '4', 'private', 'withdraw', '1000.00', 'EUR']],
        ];
    }
}
