<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Entities\Operation;
use CommissionTask\Entities\Transaction;
use CommissionTask\Entities\User;
use CommissionTask\Services\CommissionFee\Strategies\DefaultStrategy;
use CommissionTask\Services\CommissionFee\Strategies\WithdrawPrivateStrategy;
use CommissionTask\Services\CommissionFee\StrategyResolverService;
use CommissionTask\Tests\BasePhpUnit;

class StrategyResolverServiceTest extends BasePhpUnit
{
    private StrategyResolverService $strategyResolverService;

    public function setUp(): void
    {
        parent::setUp();
        $this->strategyResolverService = $this->container->get(StrategyResolverService::class);
    }

    /**
     * @dataProvider dataProviderResolve
     */
    public function testResolve(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currency,
        string $expectation
    ) {
        $this->assertEquals(
            $expectation,
            $this->strategyResolverService->getClass(
                new Transaction(
                    new Operation(
                        $date,
                        $operationType,
                        $amount,
                        $currency
                    ),
                    new User($userId, $userType)
                )
            )
        );
    }

    public function dataProviderResolve(): array
    {
        return [
            'business deposit' => [
                '2016-01-05',
                '1',
                'business',
                'deposit',
                '200.00',
                'EUR',
                DefaultStrategy::class,
            ],
            'private deposit' => [
                '2016-01-05',
                '1',
                'private',
                'deposit',
                '200.00',
                'EUR',
                DefaultStrategy::class,
            ],
            'business withdraw' => [
                '2016-02-19',
                '3',
                'business',
                'withdraw',
                '1000',
                'EUR',
                DefaultStrategy::class,
            ],
            'private withdraw' => [
                '2016-02-19',
                '5',
                'private',
                'withdraw',
                '3000000',
                'JPY',
                WithdrawPrivateStrategy::class,
            ],
        ];
    }
}
