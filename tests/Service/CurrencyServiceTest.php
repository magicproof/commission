<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Services\CurrencyServiceInterface;
use CommissionTask\Tests\BasePhpUnit;

class CurrencyServiceTest extends BasePhpUnit
{
    private CurrencyServiceInterface $currencyService;

    public function setUp(): void
    {
        parent::setUp();
        $this->currencyService = $this->container->get(CurrencyServiceInterface::class);
    }

    /**
     * @dataProvider dataProviderConvertToBaseCurrency
     */
    public function testConvertToBaseCurrency(
        string $amount,
        string $fromCurrency,
        string $expected,
    ) {
        $this->assertEquals(
            $expected,
            $this->currencyService->convertToBaseCurrency(
                $amount,
                $fromCurrency
            )
        );
    }

    public function dataProviderConvertToBaseCurrency(): array
    {
        return [
            'test EUR' => ['100', 'EUR', '100.00000'],
            'test JPY' => ['100', 'JPY', '0.77202'],
            'test USD' => ['100', 'USD', '86.97921'],
        ];
    }

    /**
     * @dataProvider dataProviderConvertFromBaseToCurrency
     */
    public function testConvertFromBaseToCurrency(
        string $amount,
        string $fromCurrency,
        string $expected
    ) {
        $this->assertEquals(
            $expected,
            $this->currencyService->convertFromBaseToCurrency(
                $amount,
                $fromCurrency
            )
        );
    }

    public function dataProviderConvertFromBaseToCurrency(): array
    {
        return [
            'test EUR' => ['100', 'EUR', '100.00000'],
            'test JPY' => ['100', 'JPY', '12953.00000'],
            'test USD' => ['100', 'USD', '114.97000'],
        ];
    }
}
