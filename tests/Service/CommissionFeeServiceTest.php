<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Services\CommissionFee\CommissionFeeInterface;
use CommissionTask\Entities\Operation;
use CommissionTask\Entities\Transaction;
use CommissionTask\Entities\User;
use CommissionTask\Tests\BasePhpUnit;

class CommissionFeeServiceTest extends BasePhpUnit
{
    private CommissionFeeInterface $commissionFeeService;

    public function setUp(): void
    {
        parent::setUp();
        $this->commissionFeeService = $this->container->get(CommissionFeeInterface::class);
    }

    /**
     * @dataProvider dataProviderCalculateCharge
     */
    public function testCalculateCharge(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currency,
        string $expectation
    ) {
        $this->assertEquals(
            $expectation,
            $this->commissionFeeService->calculateCharge(
                new Transaction(
                    new Operation(
                        $date,
                        $operationType,
                        $amount,
                        $currency
                    ),
                    new User($userId, $userType)
                )
            )
        );
    }

    public function dataProviderCalculateCharge(): array
    {
        return [
            'private deposit' => ['2016-01-05', '1', 'private', 'deposit', '200.00', 'EUR', '0.06'],
            'private withdraw JPY' => ['2016-02-19', '5', 'private', 'withdraw', '3000000', 'JPY', '8612'],
            'private withdraw free' => ['2016-02-19', '3', 'private', 'withdraw', '1000', 'EUR', '0.00'],
            'private withdraw round1' => ['2014-12-31', '4', 'private', 'withdraw', '1004.80', 'EUR', '0.02'],
            'private withdraw round2' => ['2014-12-31', '4', 'private', 'withdraw', '1000.04', 'EUR', '0.00'],
            'private withdraw round3' => ['2014-12-31', '4', 'private', 'withdraw', '1000.01', 'EUR', '0.00'],
        ];
    }
}
