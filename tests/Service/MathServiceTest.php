<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Services\MathInterface;
use CommissionTask\Services\MathService;
use CommissionTask\Tests\BasePhpUnit;
use DivisionByZeroError;

class MathServiceTest extends BasePhpUnit
{
    public const DEFAULT_SCALE = 5;

    private MathInterface $mathService;

    public function setUp(): void
    {
        parent::setUp();
        $this->mathService = new MathService(self::DEFAULT_SCALE);
    }

    public function testDivException()
    {
        $this->expectException(
            DivisionByZeroError::class,
        );
        $this->mathService->div('5', '0');
    }

    /**
     * @dataProvider dataProviderForOutputTest
     */
    public function testFormatOuput(string $amount, string $expectation, int $scale)
    {
        $this->assertEquals(
            $expectation,
            $this->mathService->formatOutput($amount, $scale),
        );
    }

    public function dataProviderForOutputTest(): array
    {
        return [
            'format natural number 1' => ['3.0201', '3.02', 2],
            'format natural number 2' => ['3.021', '3.03', 2],
            'format negative number' => ['-2', '-2.00000', 5],
            'format float' => ['3.05123', '3.06', 2],
            'format zero number' => ['0', '0.00', 2],
        ];
    }

    /**
     * @dataProvider dataProviderForAddTest
     */
    public function testAdd(string $leftOperand, string $rightOperand, string $expectation, ?int $scale = null)
    {
        if ($scale) {
            $this->mathService->setScale($scale);
        }
        $this->assertEquals(
            $expectation,
            $this->mathService->add($leftOperand, $rightOperand),
        );
    }

    /**
     * @dataProvider dataProviderForSubTest
     */
    public function testSub(string $leftOperand, string $rightOperand, string $expectation, ?int $scale = null)
    {
        if ($scale) {
            $this->mathService->setScale($scale);
        }
        $this->assertEquals(
            $expectation,
            $this->mathService->sub($leftOperand, $rightOperand),
        );
    }

    /**
     * @dataProvider dataProviderForMulTest
     */
    public function testMul(string $leftOperand, string $rightOperand, string $expectation, ?int $scale = null)
    {
        if ($scale) {
            $this->mathService->setScale($scale);
        }
        $this->assertEquals(
            $expectation,
            $this->mathService->mul($leftOperand, $rightOperand),
        );
    }

    /**
     * @dataProvider dataProviderForDivTest
     */
    public function testDiv(string $leftOperand, string $rightOperand, string $expectation, ?int $scale = null)
    {
        if ($scale) {
            $this->mathService->setScale($scale);
        }
        $this->assertEquals(
            $expectation,
            $this->mathService->div($leftOperand, $rightOperand),
        );
    }

    public function dataProviderForAddTest(): array
    {
        return [
            'add two natural numbers' => ['2', '3', '5.0', 1],
            'add negative number to a positive' => ['-2', '5', '3.00000'],
            'add natural number to a float' => ['2', '1.05123', '3.051', 3],
            'add zero number to a float' => ['0', '0.023', '0.02', 2],
        ];
    }

    public function dataProviderForMulTest(): array
    {
        return [
            'mul two natural numbers' => ['2', '3', '6.0', 1],
            'mul negative number to a positive' => ['-2', '5', '-10.00000'],
            'mul natural number to a float' => ['3', '1.05123', '3.153', 3],
            'mul zero number to a float' => ['0', '0.023', '0.00', 2],
        ];
    }

    public function dataProviderForSubTest(): array
    {
        return [
            'sub two natural numbers' => ['5', '3', '2.0', 1],
            'sub negative number from a positive' => ['-2', '5', '-7.00000'],
            'sub positive number from a negative' => ['2', '-5', '7.00000'],
            'sub natural number from a float' => ['3', '1.05123', '1.948', 3],
            'sub zero number from a float' => ['0', '0.023', '-0.02', 2],
        ];
    }

    public function dataProviderForDivTest(): array
    {
        return [
            'divide two natural numbers' => ['6', '3', '2.0', 1],
            'divide negative number on positive' => ['-5', '5', '-1.00000'],
            'divide positive number on a negative' => ['2', '-2', '-1.00000'],
            'divide natural number on a float' => ['3', '1.05123', '2.853', 3],
            'divide zero number on a float' => ['0', '0.023', '0.00', 2],
        ];
    }
}
