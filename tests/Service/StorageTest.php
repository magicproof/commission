<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Storage\StorageInterface;
use CommissionTask\Storage\Storage;
use CommissionTask\Tests\BasePhpUnit;
use stdClass;

class StorageTest extends BasePhpUnit
{
    private StorageInterface $storage;

    public function setUp(): void
    {
        parent::setUp();
        $this->storage = new Storage();
    }

    public function testFind()
    {
        $object = new stdClass();
        $this->storage->attach(
            $object,
            'test'
        );
        $this->assertEquals($object, $this->storage->find('test'));
    }

    public function testCount()
    {
        $object1 = new stdClass();
        $this->storage->attach(
            $object1,
            'test1'
        );
        $object2 = new stdClass();
        $this->storage->attach(
            $object2,
            'test2'
        );
        $this->assertEquals(2, $this->storage->count());
    }

    public function testWhereKey()
    {
        $object1 = new stdClass();
        $this->storage->attach(
            $object1,
            'test1'
        );
        $object2 = new stdClass();
        $this->storage->attach(
            $object2,
            'test1'
        );
        $object3 = new stdClass();
        $this->storage->attach(
            $object3,
            'test2'
        );
        $this->assertEquals(2, $this->storage->whereKey('test1')->count());
    }
}
