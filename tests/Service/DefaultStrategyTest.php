<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Entities\Operation;
use CommissionTask\Entities\Transaction;
use CommissionTask\Entities\User;
use CommissionTask\Services\CommissionFee\Strategies\DefaultStrategy;
use CommissionTask\Tests\BasePhpUnit;

class DefaultStrategyTest extends BasePhpUnit
{
    private DefaultStrategy $defaultStrategy;

    public function setUp(): void
    {
        parent::setUp();
        $this->defaultStrategy = $this->container->get(DefaultStrategy::class);
    }

    /**
     * @dataProvider dataProviderCalculateCharge
     */
    public function testCalculateCharge(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currency,
        string $expectation
    ) {
        $this->assertEquals(
            $expectation,
            $this->defaultStrategy->getChargeAmount(
                new Transaction(
                    new Operation(
                        $date,
                        $operationType,
                        $amount,
                        $currency
                    ),
                    new User($userId, $userType)
                ),
            )
        );
    }

    public function dataProviderCalculateCharge(): array
    {
        return [
            'private deposit' => ['2016-01-05', '1', 'private', 'deposit', '200.00', 'EUR', '200.00'],
            'business withdraw ' => ['2016-01-06', '2', 'business', 'withdraw', '1100.00', 'EUR', '1100.00'],
        ];
    }
}
