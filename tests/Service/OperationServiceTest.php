<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Services\OperationServiceInterface;
use CommissionTask\Entities\Operation;
use CommissionTask\Exceptions\ValidationException;
use CommissionTask\Tests\BasePhpUnit;

class OperationServiceTest extends BasePhpUnit
{
    private OperationServiceInterface $operationService;

    public function setUp(): void
    {
        parent::setUp();
        $this->operationService = $this->container->get(OperationServiceInterface::class);
    }

    /**
     * @dataProvider dataProviderGetOperation
     */
    public function testGetOperation(array $row)
    {
        $this->assertInstanceOf(Operation::class, $this->operationService->getOperation($row));
    }

    public function testNotValidaData()
    {
        $this->expectException(
            ValidationException::class,
        );
        $this->operationService->getOperation(['4', '1200.00']);
    }

    public function dataProviderGetOperation(): array
    {
        return [
            'test 1' => [['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'EUR']],
            'test 2' => [['2015-01-01', '4', 'private', 'withdraw', '1000.00', 'EUR']],
        ];
    }
}
