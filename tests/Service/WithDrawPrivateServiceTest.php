<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Entities\Operation;
use CommissionTask\Entities\Transaction;
use CommissionTask\Entities\User;
use CommissionTask\Services\WithDrawPrivateService;
use CommissionTask\Tests\BasePhpUnit;

class WithDrawPrivateServiceTest extends BasePhpUnit
{
    private WithDrawPrivateService $withDrawPrivateService;

    public function setUp(): void
    {
        parent::setUp();
        $this->withDrawPrivateService = $this->container->get(WithDrawPrivateService::class);
    }

    /**
     * @dataProvider dataProviderGetAmountToCharge
     */
    public function testGetAmountToCharge(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currency,
        string $expectation
    ) {
        $this->assertEquals(
            $expectation,
            $this->withDrawPrivateService->getAmountToCharge(
                new Transaction(
                    new Operation(
                        $date,
                        $operationType,
                        $amount,
                        $currency
                    ),
                    new User($userId, $userType)
                )
            )
        );
    }

    public function dataProviderGetAmountToCharge(): array
    {
        return [
            'private withdraw 1' => ['2014-12-31', '4', 'private', 'withdraw', '1200.00', 'EUR', '200.00000'],
            'private withdraw 2' => ['2015-01-01', '4', 'private', 'withdraw', '1000.00', 'EUR', '0.00000'],
            'private withdraw JPY' => ['2016-02-19', '5', 'private', 'withdraw', '3000000', 'JPY', '2870469.99965'],
        ];
    }
}
