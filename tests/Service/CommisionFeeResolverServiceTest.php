<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Contracts\Config\ConfigInterFace;
use CommissionTask\Entities\Operation;
use CommissionTask\Entities\Transaction;
use CommissionTask\Entities\User;
use CommissionTask\Services\CommissionFee\CommissionFeeResolverService;
use CommissionTask\Tests\BasePhpUnit;

class CommisionFeeResolverServiceTest extends BasePhpUnit
{
    private CommissionFeeResolverService $commisionFeeResolverService;
    private ConfigInterFace              $config;

    public function setUp(): void
    {
        parent::setUp();
        $this->commisionFeeResolverService = $this->container->get(CommissionFeeResolverService::class);
        $this->config = $this->container->get(ConfigInterFace::class);
    }

    /**
     * @dataProvider dataProviderResolve
     */
    public function testResolve(
        string $date,
        string $userId,
        string $userType,
        string $operationType,
        string $amount,
        string $currency,
        int $expectation
    ) {
        $this->assertEquals(
            $this->config->get('supported_commission_fee_coefficients')[$expectation],
            $this->commisionFeeResolverService->getFeeCoefficient(
                new Transaction(
                    new Operation(
                        $date,
                        $operationType,
                        $amount,
                        $currency
                    ),
                    new User($userId, $userType)
                )
            )
        );
    }

    public function dataProviderResolve(): array
    {
        return [
            'business deposit' => [
                '2016-01-05',
                '1',
                'business',
                'deposit',
                '200.00',
                'EUR',
                CommissionFeeResolverService::DEPOSIT_TYPE,
            ],
            'private deposit' => [
                '2016-01-05',
                '1',
                'private',
                'deposit',
                '200.00',
                'EUR',
                CommissionFeeResolverService::DEPOSIT_TYPE,
            ],
            'business withdraw' => [
                '2016-02-19',
                '3',
                'business',
                'withdraw',
                '1000',
                'EUR',
                CommissionFeeResolverService::WITHDRAW_BUSINESS_TYPE,
            ],
            'private withdraw' => [
                '2016-02-19',
                '5',
                'private',
                'withdraw',
                '3000000',
                'JPY',
                CommissionFeeResolverService::WITHDRAW_PRIVATE_TYPE,
            ],
        ];
    }
}
