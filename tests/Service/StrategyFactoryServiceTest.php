<?php

declare(strict_types=1);

namespace CommissionTask\Tests\Service;

use CommissionTask\Services\CommissionFee\Strategies\DefaultStrategy;
use CommissionTask\Services\CommissionFee\Strategies\WithdrawPrivateStrategy;
use CommissionTask\Services\CommissionFee\StrategyFactoryService;
use CommissionTask\Tests\BasePhpUnit;

class StrategyFactoryServiceTest extends BasePhpUnit
{
    private StrategyFactoryService $strategyFactoryService;

    public function setUp(): void
    {
        parent::setUp();
        $this->strategyFactoryService = $this->container->get(StrategyFactoryService::class);
    }

    /**
     * @dataProvider dataProviderGetStrategy
     */
    public function testGetStrategy(
        string $strategy,
    ) {
        $this->assertInstanceOf(
            $strategy,
            $this->strategyFactoryService->getStrategy(
                $strategy
            )
        );
    }

    public function dataProviderGetStrategy(): array
    {
        return [
            'defaultStrategy' => [DefaultStrategy::class],
            'withdrawPrivateStrategy' => [WithdrawPrivateStrategy::class],
        ];
    }

    public function testGetNotAvailable()
    {
        $this->assertNull(
            $this->strategyFactoryService->getStrategy(
                'test'
            )
        );
    }
}
