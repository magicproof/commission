<?php

declare(strict_types=1);

namespace CommissionTask\Tests;

use CommissionTask\Bootstrap;
use CommissionTask\Contracts\Services\ApiExchangeRatesInterface;
use CommissionTask\Services\ApiExchangeRatesService;
use DI\Container;
use PHPUnit\Framework\TestCase;

class BasePhpUnit extends TestCase
{
    protected Container $container;

    public function setUp(): void
    {
        parent::setUp();
        $bootstrap = new Bootstrap();
        $bootstrap->resetContainer();
        $this->container = $bootstrap->getContainer();
        $this->mockRateService();
    }

    protected function mockRateService(): void
    {
        $rateService = $this->createMock(ApiExchangeRatesService::class);
        $rateService
            ->method('getLatest')
            ->willReturn(
                [
                    'EUR' => 1,
                    'USD' => 1.1497,
                    'JPY' => 129.53,
                ]
            );

        $this->container->set(ApiExchangeRatesInterface::class, $rateService);
    }
}
