<?php

declare(strict_types=1);

require __DIR__ . '/../vendor/autoload.php';

use CommissionTask\Commands\CalculateCommission;
use Symfony\Component\Console\Application;

$app = new Application();

$app->add(new CalculateCommission());
$app->run();